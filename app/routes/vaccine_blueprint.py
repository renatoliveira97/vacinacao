from flask import Blueprint
from app.controllers.vaccine_controller import (
    create_vaccine_card,
    get_all
)


bp: Blueprint = Blueprint("bp_vaccine_cards", __name__, url_prefix="/vaccinations")

bp.post('')(create_vaccine_card)
bp.get('')(get_all)