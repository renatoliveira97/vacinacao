from flask import Flask
from app.routes.vaccine_blueprint import bp as bp_vaccine_cards


def init_app(app: Flask):
    app.register_blueprint(bp_vaccine_cards)
    