from flask import jsonify, request, current_app
from app.models.vaccine_model import VaccineCard
from app.exceptions.vaccine_exceptions import (
    MissingKeyException,
    CpfLengthIsNotValidException,
    CpfIsNotANumberException,
    InvalidValuesException,
    CpfAlreadyExistsException
)


def create_vaccine_card():

    try:

        data = request.get_json()

        data = VaccineCard.check_missing_keys(data)

        VaccineCard.check_cpf(data)

        VaccineCard.check_if_cpf_exists(data)

        VaccineCard.check_if_values_are_string(data)

        data = VaccineCard.normalize_data(data)

        data = VaccineCard.add_shots_data(data)

        vaccine_card: VaccineCard = VaccineCard(**data)

        current_app.db.session.add(vaccine_card)
        current_app.db.session.commit()

        return jsonify(vaccine_card), 201

    except MissingKeyException as e:

        return jsonify({
            "error": e.message
        }), 400

    except CpfLengthIsNotValidException as e:
        
        return jsonify({
            "error": e.message
        }), 400

    except CpfIsNotANumberException as e:

        return jsonify({
            "error": e.message
        }), 400

    except InvalidValuesException as e:

        return jsonify({
            "error": e.message
        }), 400
 
    except CpfAlreadyExistsException as e:

        return jsonify({
            "error": e.message
        }), 409


def get_all():

    vaccine_cards = VaccineCard.query.all()

    return jsonify(vaccine_cards), 200
