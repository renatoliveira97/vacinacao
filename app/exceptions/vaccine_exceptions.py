class CustomizedException(Exception):
    
    def __init__(self, message):
        self.message = message


    def get_message(self):
        return self.message


class MissingKeyException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class CpfLengthIsNotValidException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class CpfIsNotANumberException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class InvalidValuesException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)


class CpfAlreadyExistsException(CustomizedException):

    def __init__(self, message):
        CustomizedException.__init__(self, message)
