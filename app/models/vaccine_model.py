from app.configs.database import db
from dataclasses import dataclass
from datetime import datetime, timedelta
from app.exceptions.vaccine_exceptions import (
    MissingKeyException,
    CpfLengthIsNotValidException,
    CpfIsNotANumberException,
    InvalidValuesException,
    CpfAlreadyExistsException
)


@dataclass
class VaccineCard(db.Model):

    cpf: str
    name: str
    first_shot_date: datetime
    second_shot_date: datetime
    vaccine_name: str
    health_unit_name: str

    __tablename__ = 'vaccine_cards'

    cpf = db.Column(db.String(11), primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    first_shot_date = db.Column(db.DateTime)
    second_shot_date = db.Column(db.DateTime)
    vaccine_name = db.Column(db.String(50), nullable = False)
    health_unit_name = db.Column(db.String(50))


    @staticmethod
    def check_missing_keys(data: dict) -> dict:

        required_keys: list = ['cpf', 'name', 'vaccine_name', 'health_unit_name']
        missing_keys: list = []
        invalid_keys: list = []

        for key in required_keys:
            if key not in data:
                missing_keys.append(key)

        if missing_keys != []:
            raise MissingKeyException(f'The keys {missing_keys} are missing.')
        else:
            for key in data:
                if key not in required_keys:
                    invalid_keys.append(key)

            for key in invalid_keys:
                data.pop(key)

        return data


    @staticmethod
    def check_cpf(data: dict) -> None:

        cpf = data['cpf']

        if len(cpf) != 11:
            raise CpfLengthIsNotValidException("The cpf's length is not 11.")

        try:
            int(cpf)
        except ValueError:
            raise CpfIsNotANumberException('The given cpf is not a number.')


    @staticmethod
    def check_if_values_are_string(data: dict) -> None:

        invalid_values = []

        for value in data.values():
            if not isinstance(value, str):
                invalid_values.append(value)

        if invalid_values != []:
            raise InvalidValuesException(f'The values {invalid_values} are not instances of type string.')


    @staticmethod
    def normalize_data(data: dict) -> dict:

        data["name"] = data["name"].title()
        data["vaccine_name"] = data["vaccine_name"].title()
        data["health_unit_name"] = data["health_unit_name"].title()

        return data


    @staticmethod
    def add_shots_data(data: dict) -> dict:

        data["first_shot_date"] = datetime.now()
        data["second_shot_date"] = datetime.now() + timedelta(days=90)

        return data


    @staticmethod
    def check_if_cpf_exists(data: dict) -> None:

        vaccine_card = VaccineCard.query.filter_by(cpf = data["cpf"]).first()

        if vaccine_card is not None:
            raise CpfAlreadyExistsException('This cpf already exists.')
